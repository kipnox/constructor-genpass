#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

const char all_symbol[] = {'A','a','B','b','C','c','D','d','F','f','G','g','H','h','J','j',
					'K','k','L','l','M','m','N','n','P','p','Q','q','R','r','S','s','T','t','V','v','W','w',
					'X','x','Y','y','Z','z','1','2','3','4','5','6','7','8','9','0',
					'~','!','@','#','$','%','^','&','*','(',')','-','+','='}; // an Array symbol

const char lettersBig[] = {'a','b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x',
						'y','z'};

const char lettersSmall[] = {'A','B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X',
						'Y','Z'};

const char numbers[] = {'1','2','3','4','5','6','7','8','9','0'};
const int letters = 44;
const int numLetters = 54;

int enter_symbol() // Enter symbol qantity | logic 
{
	int symbol;
	cout << "Enter symbol quantity: ";
	cin >> symbol;
	return symbol;
}

int enter_category()
{
	int category;
	cout << "| Enter category: 1) Letters Big 2) Letters Small 3) All Letters  4) Numbers |" << "\n" <<
							"| 5) Number and Letters 6) All Sybmol 7) Exit Application |" << endl;
	cout << "Enter: ";
	cin >> category;
	return category;
}

int logic(int symbol, int category) // generation password logic
{
	srand(time(NULL));
	int sizeLettersBig = sizeof(lettersBig);
	int sizeLettersSmall = sizeof(lettersSmall);
	int sizeNumber = sizeof(numbers);
	int sizeSymbol = sizeof(all_symbol);
	for (int i = 1; i <= symbol; ++i)
	{
		switch(category)
		{
			case 1:
				cout << lettersBig[rand() % sizeLettersBig];
			break;
			case 2:
				cout << lettersSmall[rand() % sizeLettersSmall];
			break;
			case 3:
				cout << all_symbol[rand() % letters];
			break;
			case 4:
				cout << numbers[rand() % sizeNumber];
			break;
			case 5:
				cout << all_symbol[rand() % numLetters];
			break;
			case 6:
				cout << all_symbol[rand() % sizeSymbol];
			break;

		}
	}
	if (category == 7)
	{
		cout << "Exit Application..." << endl;
	}
	return 0;
}


int main() // main
{
	int function_enter = enter_symbol();
	int function_enter_category = enter_category();
	int function_logic = logic(function_enter,function_enter_category);

	cout << endl;

	return 0;
}